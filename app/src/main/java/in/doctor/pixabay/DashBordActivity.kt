package `in`.doctor.pixabay

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class DashBordActivity : AppCompatActivity() {
    lateinit var frameLayout: FrameLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash_bord)
        frameLayout = findViewById(R.id.fragment_frameLayout)

        setupNavigationView()
    }

    private fun setupNavigationView() {
        val bottomNavigationView: BottomNavigationView? =
            findViewById(R.id.bottom_navigation) as BottomNavigationView?
        if (bottomNavigationView != null) {
            // Select first menu item by default and show Fragment accordingly.
            val menu: Menu = bottomNavigationView.getMenu()
            selectFragment(menu.getItem(0))

            // Set action to perform when any menu-item is selected.
            bottomNavigationView.setOnNavigationItemSelectedListener(
                object : BottomNavigationView.OnNavigationItemSelectedListener {
                    override fun onNavigationItemSelected(item: MenuItem): Boolean {
                        selectFragment(item)
                        return false
                    }
                })
        }
    }

    protected fun selectFragment(item: MenuItem) {
        item.isChecked = true
        when (item.itemId) {
            R.id.home_nav ->                 // Action to perform when Home Menu item is selected.
                pushFragment(HomeFragment())
            R.id.connect_nav ->               //  Toast.makeText(this, "work is under progress", Toast.LENGTH_SHORT).show();
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_frameLayout, ServiceFragment(), "loanno").commit()
            R.id.appointment_nav ->                 //Toast.makeText(this, "work is under progress", Toast.LENGTH_SHORT).show();
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_frameLayout, NotificationFragment()).commit()
        }

    }

    fun pushFragment(fragment: Fragment?) {
        if (fragment == null) return
        val fragmentManager = fragmentManager
        if (fragmentManager != null) {
            val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
            if (transaction != null) {
                transaction.replace(R.id.fragment_frameLayout, fragment, "aman")
                transaction.commit()
            }
        }
    }
}