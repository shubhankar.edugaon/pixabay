package doctor.pixabay

import `in`.doctor.pixabay.R
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.getSystemService
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_item.view.*
import javax.microedition.khronos.opengles.GL

class MainAdapter(var imageList: List<imageModel>, private var onItemClickListener: OnItemClickListener): RecyclerView.Adapter<MainAdapter.imageViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): imageViewHolder {
        return imageViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_item, parent, false), onItemClickListener)
    }

    override fun onBindViewHolder(holder: imageViewHolder, position: Int) {
            holder.bind(imageList[position])
        val data =imageList[position]
        holder.imageListHolder = data
    }

    override fun getItemCount(): Int {
        return imageList.size
    }

    class imageViewHolder(itemView: View, onItemClickListener: OnItemClickListener): RecyclerView.ViewHolder(itemView) {
        lateinit var imageListHolder: imageModel
        fun bind(imageModel: imageModel){
            val name = itemView.findViewById<TextView>(R.id.textView)
            name.text = imageModel.tags
            Glide.with(itemView.context).load(imageModel.previewURL).into(itemView.flower_imageView)
        }
        init {
            itemView.setOnClickListener {
             onItemClickListener.imageClick(imageListHolder)
            }
        }
    }
interface OnItemClickListener{
        fun imageClick(imageModel: imageModel)
}
}