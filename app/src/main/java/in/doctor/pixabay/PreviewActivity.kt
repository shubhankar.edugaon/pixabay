package `in`.doctor.pixabay

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_preview.*

class PreviewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview)
        arrow_back.setOnClickListener {
            onBackPressed()
        }
        val previewImage = intent.getStringExtra("previewURL")
        val webFromateUrl = intent.getStringExtra("webformatURL")
        val largeImageUrl = intent.getStringExtra("largeImageURL")
        val userImage = intent.getStringExtra("userImageURL")
        val  imageName = intent.getStringExtra("imageName")
        image_name.text = imageName
        Glide.with(this).load(largeImageUrl).into(image_view)
        Glide.with(this).load(webFromateUrl).into(webFormateImage)
        Glide.with(this).load(previewImage).into(second_image)
    }
}