package doctor.pixabay

import com.android.volley.toolbox.StringRequest

data class imageModel(
    var tags: String? = null,
    var previewURL: String? = null,
    var webformatURL:String? = null,
    var largeImageURL:String? =  null,
    var userImageURL: String?  = null
)
