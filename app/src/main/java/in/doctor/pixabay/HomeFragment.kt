package `in`.doctor.pixabay

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import doctor.pixabay.MainAdapter
import doctor.pixabay.imageModel
import org.json.JSONArray


class HomeFragment : Fragment(), MainAdapter.OnItemClickListener {
    private var imageList = ArrayList<imageModel>()
    lateinit var recyClerView: RecyclerView
    var progressDialog: ProgressDialog? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        recyClerView = view.findViewById(R.id.recyclerView)

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        api()
    }
    private fun api(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(activity)
        val url = "https://pixabay.com/api/?key=27540057-450087193c6db511a6c3974c4&q=yellow+animals&image_type=photo&pretty=true"
        val strReq: StringRequest = object : StringRequest(
            Method.POST, url, Response.Listener { response ->
            Log.e(TAG, "response: " + response)
                progressDialog!!.dismiss()
            try {
                val jsonArray = JSONArray("[$response]")
                val message = jsonArray.getJSONObject(0).get("hits") as JSONArray
                //val messageObj = jsonArray.getJSONObject(0).get("hits") as JSONArray
                for (i in 0 until message.length()) {
                    val previewUrl = message.getJSONObject(i).getString("previewURL")
                    val  webFormateUrl = message.getJSONObject(i).getString("webformatURL")
                    val largeImage = message.getJSONObject(i).getString("largeImageURL")
                    val userUrl =  message.getJSONObject(i).getString("userImageURL")
                    val tags = message.getJSONObject(i).getString("tags")
                    var image = imageModel(tags,previewUrl,webFormateUrl,largeImage,userUrl)
                    imageList.add(image)
                    showSlotsList(imageList)
                }


            } catch (e: Exception) { // caught while parsing the response
                progressDialog!!.dismiss()
                //dismissLoaderActivity()
                Toast.makeText(activity, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }

        },
            Response.ErrorListener { volleyError -> // error occurred
                progressDialog!!.dismiss()
                //dismissLoaderActivity()
                Toast.makeText(activity, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()

                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        progressDialog = ProgressDialog(activity, androidx.appcompat.R.style.AlertDialog_AppCompat)
        progressDialog!!.setMessage("Checking Availability...")
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
    }
    private fun showSlotsList(imageList: List<imageModel>){
        val adapter = MainAdapter(imageList, this)
        val layoutManager = GridLayoutManager(activity, 3)
        recyClerView.setLayoutManager(layoutManager)
        recyClerView.setAdapter(adapter)

    }

    override fun imageClick(imageModel: imageModel) {
        val intent  =  Intent(activity,PreviewActivity::class.java)
       intent.putExtra("previewURL", imageModel.previewURL)
        intent.putExtra("webformatURL", imageModel.webformatURL)
        intent.putExtra("largeImageURL", imageModel.largeImageURL)
        intent.putExtra("userImageURL", imageModel.userImageURL)
        intent.putExtra("imageName", imageModel.tags)
        startActivity(intent)
        //Toast.makeText(activity,"${imageModel.tags}", Toast.LENGTH_SHORT).show()
    }
}